#pragma once

#include <glm/mat3x3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace transform2D
{
inline glm::mat3 scale(glm::vec2 const& v)
{
    glm::mat3 m;
    m[0][0] = v.x;
    m[1][1] = v.y;
    return m;
}
inline glm::mat3 translate(glm::vec2 const& v)
{
    glm::mat3 m;
    m[2][0] = v.x;
    m[2][1] = v.y;
    return m;
}
// scale -> translate
inline glm::mat3 TS(glm::vec2 const& t, glm::vec2 const& s)
{
    glm::mat3 m;
    m[0][0] = s.x;
    m[1][1] = s.y;
    m[2][0] = t.x;
    m[2][1] = t.y;
    return m;
}
}
